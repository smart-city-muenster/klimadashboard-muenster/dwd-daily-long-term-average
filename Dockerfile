FROM python:3.12-slim-bullseye

# install gcc
RUN apt-get update && apt-get install -y gcc

WORKDIR /app

COPY ./app/requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./app /app
COPY ./data/daily-long-term-average.json /app/daily-long-term-average.json

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "80"]