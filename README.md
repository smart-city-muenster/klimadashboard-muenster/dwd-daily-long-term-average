# DWD daily long term average

This is a simple Flask web application for analyzing weather data using the Wetterdienst library. The application retrieves daily air temperature data from a specified weather station and calculates the difference between the current average and the long-term average for a given date.
