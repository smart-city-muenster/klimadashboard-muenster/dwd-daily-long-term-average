import datetime
import logging
import pandas as pd
from fastapi import FastAPI, Depends, HTTPException, Query

from fastapi_cache import FastAPICache
from fastapi_cache.backends.inmemory import InMemoryBackend
from fastapi_cache.decorator import cache

from wetterdienst import Settings
from wetterdienst.provider.dwd.observation import DwdObservationRequest, DwdObservationParameter, DwdObservationResolution

DAILY_LONG_TERM_AVERAGE_PATH = "daily-long-term-average.json"

logging.basicConfig(filename='record.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

app = FastAPI()

async def get_request_date(date: str = Query(None, description="Requested date in the format YYYY-MM-DD")):
    if date is None:
        return datetime.date.today()
    try:
        return datetime.datetime.strptime(date, "%Y-%m-%d")
    except ValueError:
        raise HTTPException(status_code=400, detail="Invalid date format. Please use YYYY-MM-DD")

@app.get("/", response_model=dict)
@cache(expire=60*60)
async def main(date: datetime.date = Depends(get_request_date)):
    """
    Main function for handling the root route ("/") of the server.
    Retrieves the requested date and calculates the difference between the average air temperature
    for the requested date and the long-term average air temperature for that specific day of the year.
    Returns the calculated difference as a JSON response.

    :return: JSON response containing the difference between the current and long-term average air temperature
    :rtype: dict
    """
    settings = Settings(
        ts_shape="long",
        ts_humanize=True,
        ts_si_units=False
    )

    dwd_request = DwdObservationRequest(
        parameter=DwdObservationParameter.DAILY.TEMPERATURE_AIR_MEAN_200,
        resolution=DwdObservationResolution.DAILY,
        start_date=datetime.datetime(date.year, date.month, 1, tzinfo=datetime.timezone.utc),
        end_date=datetime.datetime(date.year, date.month, date.day, tzinfo=datetime.timezone.utc),
        settings=settings
    ).filter_by_station_id(station_id=(1766))

    df = dwd_request.values.all().df.to_pandas()

    df["day"] = df["date"].dt.day
    df["month"] = df["date"].dt.month

    df_average = pd.read_json(DAILY_LONG_TERM_AVERAGE_PATH)

    cur_avg = df.dropna()["value"].mean()
    long_term_avg = df_average.loc[(df_average['month'] == date.month) & (df_average['day'] == date.day), 'value'].values[0]

    return {"date": date, "value": cur_avg - long_term_avg}

@app.on_event("startup")
async def startup():
    FastAPICache.init(InMemoryBackend())


if __name__ == '__main__':
    import uvicorn

    uvicorn.run(app)
