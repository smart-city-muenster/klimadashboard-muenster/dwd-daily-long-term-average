from wetterdienst import Settings
from wetterdienst.provider.dwd.observation import DwdObservationRequest, DwdObservationParameter, DwdObservationResolution
import pandas as pd

def create_year_df():
    # Specify the start date and the number of days
    start_date = '2024-01-01'
    num_days = 366  # or 366 for a leap year

    # Create a DataFrame with all days in the specified range
    date_range = pd.date_range(start=start_date, periods=num_days, freq='D')
    df = pd.DataFrame(date_range, columns=['Date'])

    df["day"] = df["Date"].dt.day
    df["month"] = df["Date"].dt.month

    # remove date column
    df = df.drop(columns=["Date"])

    return df


settings = Settings( # default
    ts_shape="long",  # tidy data
    ts_humanize=True,  # humanized parameters
    ts_si_units=False  # convert values to SI units
)
request = DwdObservationRequest(
   parameter=DwdObservationParameter.DAILY.TEMPERATURE_AIR_MEAN_200,
   resolution=DwdObservationResolution.DAILY,
   start_date="1960-01-01",  # if not given timezone defaulted to UTC
   end_date="1990-01-01",  # if not given timezone defaulted to UTC
   settings=settings
).filter_by_station_id(station_id=(3404))
stations = request.df

values = request.values.all().df

df = values.to_pandas()

df["day"] = df["date"].dt.day
df["month"] = df["date"].dt.month

df_year = create_year_df()
df_year["value"] = df_year.apply(lambda row: df.loc[(df['month'] == row["month"]) & (df['day'] <= row["day"]), 'value'].mean(), axis=1)

df_year.to_json("daily-long-term-average.json", orient="records")





